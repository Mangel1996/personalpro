<?php

namespace persistencia\basedatos;

use PDO;

class Conexion {

    public function conectar() {
        $cnn = new PDO('pgsql:host=localhost;port=5432;dbname=PersonalPro;', 'postgres', 'postgres');
        $cnn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $cnn;
    }

}
