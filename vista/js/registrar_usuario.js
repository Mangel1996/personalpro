
var registra = {
    init: function () {
        $('#formularioregistrar').on('submit', registra.registrar);
    },
    peticion: function (url, parametro, metodo) {
        $.ajax({
            'url': url,
            'type': 'POST',
            'data': parametro,
            'enctype': 'multipart/form-data',
            contentType: false,
            cache: false,
            processData: false,
            success: function (respuesta) {
                metodo(respuesta);
            },
            error: function (respuesta) {
                console.log(respuesta);
            }



        });
    }, peticioSencilla: function (url, parametro, metodo) {

        $.ajax({
            'url': url,
            'type': 'POST',
            'data': parametro,
            success: function (respuesta) {
                metodo(respuesta);
            },
            error: function (respuesta) {
                console.log(respuesta);
            }
        });

    },
    detener: function (e) {
        if (e.preventDefault) {
            e.preventDefault();
        }
        if (e.stopPropagation) {
            e.stopPropagation();
        }
        if (e.returnValue) {
            e.returnValue = false;
        }
    }, registrar: function (e) {
        registra.detener(e);
        var url = '/PersonalPro/index.php/usuario/guardar';
        registra.peticion(url, new FormData(this), registra.respuesta);
    }, respuesta: function (respuesta) {

        if (respuesta.codigo === 1) {

            mensaje.addClass('alert-success');
            mensaje.show('slow');
            mensaje.fadeOut(2000);
            mensaje.empty().html(respuesta.mensaje);
            registra.enviar(respuesta.codigo_usuario);
            return;
        }
        if (respuesta.codigo === 2) {

            registra.limpiarcajas();
            return;
        }
        if (respuesta.codigo === -1) {
            var mensaje = $('#mensaje');
            mensaje.addClass('alert-danger');
            mensaje.show('slow');
            mensaje.fadeOut(2000);
            mensaje.empty().html(respuesta.mensaje);
            return;
        }
        if (respuesta.codigo === -2) {
            console.log(respuesta.mensaje);
            return;
        }

    }, limpiarcajas: function () {
        $('#formularioregistrar')[0].reset();
    }, enviar: function (id) {

        var url = '/PersonalPro/index.php/menu/enviar/correo';
        var parametros = {};
        parametros.id_usuario = id;
        registra.peticioSencilla(url, parametros, registra.respuesta);

    }

};
registra.init();


